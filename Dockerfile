FROM node:alpine as build
WORKDIR '/app'
COPY package.json .
RUN npm install
COPY . .
RUN npm run build
 
FROM nginx as prod
COPY --from=0 /app/build /usr/share/nginx/html